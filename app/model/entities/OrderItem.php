<?php


namespace App\Model;

use Doctrine\ORM\Mapping as ORM;
use Kdyby;

/**
 * @ORM\Entity
 * @ORM\Table(name="order_item")
 */
class OrderItem extends Kdyby\Doctrine\Entities\BaseEntity
{
	use Kdyby\Doctrine\Entities\Attributes\Identifier;

	/**
	 * @ORM\Column(type="integer")
	 */
	protected $count;

	/**
	 * @ORM\Column(type="integer")
	 */
	protected $paid;

	/**
	 * @ORM\ManyToOne(targetEntity="Orders", inversedBy="orderItems")
	 * @ORM\JoinColumn(nullable=FALSE)
	 */
	protected $orders;

	/**
	 * @ORM\ManyToOne(targetEntity="Item", inversedBy="orderItems")
	 * @ORM\JoinColumn(nullable=FALSE)
	 */
	protected $item;

	/**
	 * @return mixed
	 */
	public function getCount()
	{
		return $this->count;
	}

	/**
	 * @param mixed $count
	 */
	public function setCount($count): void
	{
		$this->count = $count;
	}

	/**
	 * @return mixed
	 */
	public function getPaid()
	{
		return $this->paid;
	}

	/**
	 * @param mixed $paid
	 */
	public function setPaid($paid): void
	{
		$this->paid = $paid;
	}

	/**
	 * @return mixed
	 */
	public function getOrder()
	{
		return $this->orders;
	}

	/**
	 * @param mixed $order
	 */
	public function setOrder($order): void
	{
		$this->orders = $order;
	}

	/**
	 * @return mixed
	 */
	public function getItem()
	{
		return $this->item;
	}

	/**
	 * @param mixed $item
	 */
	public function setItem($item): void
	{
		$this->item = $item;
	}

}