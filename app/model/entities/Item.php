<?php


namespace App\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Kdyby;

/**
 * @ORM\Entity
 */
class Item extends Kdyby\Doctrine\Entities\BaseEntity
{
	use Kdyby\Doctrine\Entities\Attributes\Identifier;

	/**
	 * @ORM\Column(type="string", length=20)
	 */
	protected $category;

	/**
	 * @ORM\Column(type="integer")
	 */
	protected $price;

	/**
	 * @ORM\Column(type="string", length=30)
	 */
	protected $name;

	/**
	 * @ORM\OneToMany(targetEntity="OrderItem", mappedBy="item", cascade={"persist", "remove"}, orphanRemoval=TRUE)
	 */
	protected $orderItems;

	/**
	 * @param mixed $category
	 */
	public function setCategory($category): void
	{
		$this->category = $category;
	}

	/**
	 * @param mixed $price
	 */
	public function setPrice($price): void
	{
		$this->price = $price;
	}

	/**
	 * @param mixed $name
	 */
	public function setName($name): void
	{
		$this->name = $name;
	}

	/**
	 * @return mixed
	 */
	public function getCategory()
	{
		return $this->category;
	}

	/**
	 * @return mixed
	 */
	public function getPrice()
	{
		return $this->price;
	}

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @return mixed
	 */
	public function getIngredients()
	{
		return $this->ingredients;
	}

	/**
	 * @return mixed
	 */
	public function __construct()
	{
		$this->orderItems = new ArrayCollection();
	}

}