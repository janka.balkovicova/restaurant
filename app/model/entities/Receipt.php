<?php


namespace App\Model;

use Doctrine\ORM\Mapping as ORM;
use Kdyby;

/**
 * @ORM\Entity
 */
class Receipt extends Kdyby\Doctrine\Entities\BaseEntity
{
	use Kdyby\Doctrine\Entities\Attributes\Identifier;

	/**
	 * @ORM\Column(type="integer")
	 */
	protected $sum;

	/**
	 * @ORM\Column(type="string", length=20, name="payment_type")
	 */
	protected $paymentMethod;

	/**
	 * @ORM\Column(type="datetime", name="created_at")
	 * @ORM\Version
	 */
	/* The version attribute is flag to tell Doctrine that it can change
	the value in this column to keep track of where concurrent writes may
	impact the row data prior to the end of long running transaction. */
	protected $createdAt; //timestamp, kdy byla uctenka vydana

//	protected $datum;

	//FK - ID_objednavky
	//FK - ID_zamestnance

	//(0..)many to one -> zamestnanec
	/**
	 * @ORM\ManyToOne(targetEntity="Employee")
	 * @ORM\JoinColumn(nullable=false)
	 */
	protected $employee;

	//(1..)many to one -> objednavka
	/**
	 * @ORM\ManyToOne(targetEntity="Orders")
	 * @ORM\JoinColumn(nullable=false)
	 */
	protected $orders;

	/**
	 * @return mixed
	 */
	public function getSum()
	{
		return $this->sum;
	}

	/**
	 * @param mixed $sum
	 */
	public function setSum($sum): void
	{
		$this->sum = $sum;
	}

	/**
	 * @return mixed
	 */
	public function getPaymentMethod()
	{
		return $this->paymentMethod;
	}

	/**
	 * @param mixed $paymentMethod
	 */
	public function setPaymentMethod($paymentMethod): void
	{
		$this->paymentMethod = $paymentMethod;
	}

	/**
	 * @return mixed
	 */
	public function getEmployee()
	{
		return $this->employee;
	}

	/**
	 * @param mixed $employee
	 */
	public function setEmployee($employee): void
	{
		$this->employee = $employee;
	}

	/**
	 * @return mixed
	 */
	public function getOrder()
	{
		return $this->orders;
	}

	/**
	 * @param mixed $order
	 */
	public function setOrder($order): void
	{
		$this->orders = $order;
	}
}