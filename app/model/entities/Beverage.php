<?php


namespace App\Model;

use Doctrine\ORM\Mapping as ORM;
use Kdyby;

/**
 * @ORM\Entity
 */
class Beverage extends Kdyby\Doctrine\Entities\BaseEntity
{
	use Kdyby\Doctrine\Entities\Attributes\Identifier;

	/**
	 * @ORM\Column (type="decimal", precision=4, scale=2)
	 */
	protected $volume;

	/**
	 * @ORM\Column (type="decimal", precision=4, scale=2, name="alcohol_percentage")
	 */
	protected $alcoholPercentage;

	/**
	 * @ORM\OneToOne(targetEntity="Item", cascade={"persist", "remove"})
	 * @ORM\JoinColumn(nullable=false)
	 */
	protected $item;

	/**
	 * @return mixed
	 */
	public function getVolume()
	{
		return $this->volume;
	}

	/**
	 * @param mixed $volume
	 */
	public function setVolume($volume): void
	{
		$this->volume = $volume;
	}

	/**
	 * @return mixed
	 */
	public function getAlcoholPercentage()
	{
		return $this->alcoholPercentage;
	}

	/**
	 * @param mixed $alcoholPercentage
	 */
	public function setAlcoholPercentage($alcoholPercentage): void
	{
		$this->alcoholPercentage = $alcoholPercentage;
	}

	/**
	 * @return mixed
	 */
	public function getItem()
	{
		return $this->item;
	}

	/**
	 * @param mixed $item
	 */
	public function setItem($item): void
	{
		$this->item = $item;
	}
}