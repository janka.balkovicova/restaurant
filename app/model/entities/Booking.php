<?php


namespace App\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Kdyby;

/**
 * @ORM\Entity
 */
class Booking extends Kdyby\Doctrine\Entities\BaseEntity
{
	public function __construct() {
		$this->tables = new ArrayCollection();
	}

	public function addTable($table) {
		$this->tables->add($table);
	}

	use Kdyby\Doctrine\Entities\Attributes\Identifier;

	/**
	 * @ORM\Column(type="string", length=30)
	 */
	protected $customer;

	/**
	 * @ORM\Column(type="datetime", length=20, name="date_time")
	 */
	protected $dateTime;

	/**
	 * @ORM\Column(type="integer", name="number_of_people")
	 */
	protected $numberOfPeople;

	/**
	 * @ORM\Column(type="integer", name="phone_number")
	 */
	protected $phoneNumber; //bez predvolby

	/**
	 * @ORM\ManyToOne(targetEntity="Employee")
	 * @ORM\JoinColumn(nullable=false)
	 */
	protected $employee;

	/**
	 * @ORM\ManyToMany(targetEntity="Tables", mappedBy="bookings")
	 */
	protected $tables;

	/**
	 * @return mixed
	 */
	public function getCustomer()
	{
		return $this->customer;
	}

	/**
	 * @param mixed $customer
	 */
	public function setCustomer($customer): void
	{
		$this->customer = $customer;
	}

	/**
	 * @return mixed
	 */
	public function getDateTime()
	{
		return $this->dateTime;
	}

	/**
	 * @param mixed $dateTime
	 */
	public function setDateTime($dateTime): void
	{
		$this->dateTime = $dateTime;
	}

	/**
	 * @return mixed
	 */
	public function getNumberOfPeople()
	{
		return $this->numberOfPeople;
	}

	/**
	 * @param mixed $numberOfPeople
	 */
	public function setNumberOfPeople($numberOfPeople): void
	{
		$this->numberOfPeople = $numberOfPeople;
	}

	/**
	 * @return mixed
	 */
	public function getPhoneNumber()
	{
		return $this->phoneNumber;
	}

	/**
	 * @param mixed $phoneNumber
	 */
	public function setPhoneNumber($phoneNumber): void
	{
		$this->phoneNumber = $phoneNumber;
	}

	/**
	 * @return mixed
	 */
	public function getEmployee()
	{
		return $this->employee;
	}

	/**
	 * @param mixed $employee
	 */
	public function setEmployee($employee): void
	{
		$this->employee = $employee;
	}
}