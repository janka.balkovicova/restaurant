<?php


namespace App\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Kdyby;

/**
 * @ORM\Entity
 */
class Tables extends Kdyby\Doctrine\Entities\BaseEntity
{
	use Kdyby\Doctrine\Entities\Attributes\Identifier;

	/**
	 * @ORM\Column(type="integer")
	 */
	protected $capacity;

	/**
	 * @ORM\Column(type="string", length=20)
	 */
	protected $room;

	/**
	 * @ORM\ManyToOne(targetEntity="Orders", inversedBy="tables")
	 * @ORM\JoinColumn(nullable=true)
	 */
	protected $orders;

	//(1..)many to (0..)many -> rezervace
	/**
	 * @ORM\ManyToMany(targetEntity="Booking", inversedBy="tables")
	 * @ORM\JoinTable(name="tables_bookings")
	 */
	protected $bookings;

	public function __construct()
	{
		$this->bookings = new ArrayCollection();
	}

	public function addBooking(Booking $booking = null)
	{
		$this->bookings->add($booking);
	}

	/**
	 * @return mixed
	 */
	public function getCapacity()
	{
		return $this->capacity;
	}

	/**
	 * @param mixed $capacity
	 */
	public function setCapacity($capacity): void
	{
		$this->capacity = $capacity;
	}

	/**
	 * @return mixed
	 */
	public function getRoom()
	{
		return $this->room;
	}

	/**
	 * @param mixed $room
	 */
	public function setRoom($room): void
	{
		$this->room = $room;
	}

	/**
	 * @return mixed
	 */
	public function getOrder()
	{
		return $this->orders;
	}

	/**
	 * @param mixed $order
	 */
	public function setOrder($order): void
	{
		$this->orders = $order;
	}

	/**
	 * @return mixed
	 */
	public function getBookings()
	{
		return $this->bookings;
	}

	/**
	 * @param mixed $bookings
	 */
	public function setBookings($bookings): void
	{
		$this->bookings = $bookings;
	}


}