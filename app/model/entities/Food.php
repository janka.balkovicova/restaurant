<?php


namespace App\Model;

use Doctrine\ORM\Mapping as ORM;
use Kdyby;

/**
 * @ORM\Entity
 */
class Food extends Kdyby\Doctrine\Entities\BaseEntity
{
	use Kdyby\Doctrine\Entities\Attributes\Identifier;

	/**
	 * @ORM\Column (type="integer")
	 */
	protected $grams;

	/**
	 * @ORM\Column(type="string", length=50)
	 */
	protected $ingredients;

	/**
	 * @param mixed $ingredients
	 */
	public function setIngredients($ingredients): void
	{
		$this->ingredients = $ingredients;
	}

	/**
	 * @return mixed
	 */
	public function getGrams()
	{
		return $this->grams;
	}

	/**
	 * @param mixed $grams
	 */
	public function setGrams($grams): void
	{
		$this->grams = $grams;
	}

	/**
	 * @return mixed
	 */
	public function getSize()
	{
		return $this->size;
	}

	/**
	 * @param mixed $size
	 */
	public function setSize($size): void
	{
		$this->size = $size;
	}

	/**
	 * @return mixed
	 */
	public function getItem()
	{
		return $this->item;
	}

	/**
	 * @param mixed $item
	 */
	public function setItem($item): void
	{
		$this->item = $item;
	}

	/**
	 * @ORM\Column (type="string", length=20)
	 */
	protected $size;

	/**
	 * @ORM\OneToOne(targetEntity="Item", cascade={"persist", "remove"})
	 */
	protected $item;

}