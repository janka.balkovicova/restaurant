<?php


namespace App\Model;

use Doctrine\ORM\Mapping as ORM;
use Kdyby;

/**
 * @ORM\Entity
 */
class User extends Kdyby\Doctrine\Entities\BaseEntity
{
	use Kdyby\Doctrine\Entities\Attributes\Identifier;

	/**
	 * @ORM\Column(type="string", length=10, nullable=false, unique=true)
	 */
	protected $username;

	/**
	 * @ORM\Column(type="text")
	 */
	protected $password;

	/**
	 * @ORM\Column(type="string", length=50)
	 */
	protected $email;

	/**
	 * @ORM\OneToOne(targetEntity="Employee", inversedBy="user", cascade={"persist", "remove"})
	 */
	protected $employee;


	/**
	 * @return mixed
	 */
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * @param mixed $email
	 */
	public function setEmail($email): void
	{
		$this->email = $email;
	}

	/**
	 * @return mixed
	 */
	public function getUsername()
	{
		return $this->username;
	}

	/**
	 * @param mixed $username
	 */
	public function setUsername($username): void
	{
		$this->username = $username;
	}

	/**
	 * @return mixed
	 */
	public function getPassword()
	{
		return $this->password;
	}

	/**
	 * @param mixed $password
	 */
	public function setPassword($password): void
	{
		$this->password = $password;
	}

	/**
	 * @return mixed
	 */
	public function getEmployee()
	{
		return $this->employee;
	}

	/**
	 * @param mixed $employee
	 */
	public function setEmployee($employee): void
	{
		$this->employee = $employee;
	}
}
