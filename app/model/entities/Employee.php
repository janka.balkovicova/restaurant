<?php


namespace App\Model;

use Doctrine\ORM\Mapping as ORM;
use Kdyby;

/**
 * @ORM\Entity
 */
class Employee extends Kdyby\Doctrine\Entities\BaseEntity
{
	use Kdyby\Doctrine\Entities\Attributes\Identifier;

	/**
	 * @ORM\Column(type="string", length=20)
	 */
	protected $name;

	/**
	 * @ORM\Column(type="string", length=30)
	 */
	protected $surname;

	/**
	 * @ORM\Column(type="string", length=20)
	 */
	protected $position;

	/**
	 * @ORM\OneToOne(targetEntity="User", mappedBy="employee", cascade={"persist", "remove"})
	 * @ORM\JoinColumn(nullable=false)
	 */
	protected $user;

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param mixed $name
	 */
	public function setName($name): void
	{
		$this->name = $name;
	}

	/**
	 * @return mixed
	 */
	public function getSurname()
	{
		return $this->surname;
	}

	/**
	 * @param mixed $surname
	 */
	public function setSurname($surname): void
	{
		$this->surname = $surname;
	}

	/**
	 * @return mixed
	 */
	public function getPosition()
	{
		return $this->position;
	}

	/**
	 * @param mixed $position
	 */
	public function setPosition($position): void
	{
		$this->position = $position;
	}

	/**
	 * @return mixed
	 */
	public function getUser()
	{
		return $this->user;
	}

	/**
	 * @param mixed $user
	 */
	public function setUser($user): void
	{
		$this->user = $user;
	}
}