<?php


namespace App\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Kdyby;

/**
 * @ORM\Entity
 */
class Orders extends Kdyby\Doctrine\Entities\BaseEntity
{
	use Kdyby\Doctrine\Entities\Attributes\Identifier;

	/**
	 * @ORM\OneToMany(targetEntity="OrderItem", mappedBy="orders", cascade={"persist", "remove"}, orphanRemoval=TRUE)
	 */
	protected $orderItems;

	public function __construct()
	{
		$this->orderItems = new ArrayCollection();
		$this->tables = new ArrayCollection();
	}

	/**
	 * @ORM\ManyToOne(targetEntity="Employee")
	 * @ORM\JoinColumn(nullable=false)
	 */
	protected $employee;


	/**
	 * @ORM\ManyToOne(targetEntity="Booking")
	 * @ORM\JoinColumn(nullable=true)
	 */
	protected $booking;

	/**
	 * @ORM\OneToMany(targetEntity="Tables", mappedBy="orders")
	*/
	private $tables;

	public function addTable(Tables $table = null)
	{
		$this->tables->add($table);
	}

	/**
	 * @param mixed $employee
	 */
	public function setEmployee($employee): void
	{
		$this->employee = $employee;
	}

	/**
	 * @param mixed $booking
	 */
	public function setBooking($booking): void
	{
		$this->booking = $booking;
	}

	/**
	 * @return mixed
	 */
	public function getOrderItems()
	{
		return $this->orderItems;
	}

	/**
	 * @return mixed
	 */
	public function getEmployee()
	{
		return $this->employee;
	}

	/**
	 * @return mixed
	 */
	public function getBooking()
	{
		return $this->booking;
	}

}