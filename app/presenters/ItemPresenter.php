<?php


namespace App\Presenters;

use App\Model\Beverage;
use App\Model\Food;
use Nette;
use Nette\Application\UI\Form;
use App\Model\Item;

class ItemPresenter extends BasePresenter
{
	/**
	 * @inject
	 * @var \Kdyby\Doctrine\EntityManager
	 */
	public $em;
	private $dao_items;
	private $dao_beverages;
	private $dao_foods;
	private $beverage;
	private $food;
	private $sizes = ['malá', 'střední', 'velká'];
	private $beverageCategories = ['Pivo', 'Víno', 'Nealko', 'Alko', 'Horké'];
	private $foodCategories = ['Hlavní', 'Příloha', 'Polévka', 'Pizza', 'Těstoviny'];

	public function beforeRender()
	{
		parent::beforeRender();
		if(!$this->user->isInRole('owner') and !$this->user->isInRole('main_waiter')){
			$this->flashMessage('Nemáte práva k prohlížení obsahu stránek. Přihlašte se jako majitel nebo jako vedoucí číšník.');
			$this->redirect('Homepage:');
		}
	}

	public function renderDefault()	{
		$this->template->items = $this->dao_items;
		$this->template->beverages = $this->dao_beverages;
		$this->template->foods = $this->dao_foods;
		$this->template->beverageCategories = $this->beverageCategories;
		$this->template->foodCategories = $this->foodCategories;
	}

	public function actionDefault(){
		$this->dao_items = $this->em->getRepository(Item::class)->findAll();
		$this->dao_beverages = $this->em->getRepository(Beverage::class)->findAll();
		$this->dao_foods = $this->em->getRepository(Food::class)->findAll();
	}

	public function renderEditBeverage($beverageId) {
		$this->template->beverage = $this->beverage;
		$this->template->beverageCategories = $this->beverageCategories;
	}

	public function actionEditBeverage($beverageId) {
		$this->beverage = $this->em->getRepository(Beverage::class)->find($beverageId);
		$this['addBeverageItemForm']->setDefaults(array(
			'edit' => true,
			'beverageId' => $beverageId,
			'name' => $this->beverage->getItem()->getName(),
			'category' => $this->beverage->getItem()->getCategory(),
			'price' => $this->beverage->getItem()->getPrice(),
			'volume' => $this->beverage->getVolume(),
			'alcoholPercentage' => $this->beverage->getAlcoholPercentage(),
		));
	}

	public function renderEditFood($foodId){
		$this->template->food = $this->food;
		$this->template->foodCategories = $this->foodCategories;
	}

	public function actionEditFood($foodId) {
		$this->food = $this->em->getRepository(Food::class)->find($foodId);
		$this['addFoodItemForm']->setDefaults(array(
			'edit' => true,
			'foodId' => $foodId,
			'name' => $this->food->getItem()->getName(),
			'category' => $this->food->getItem()->getCategory(),
			'price' => $this->food->getItem()->getPrice(),
			'grams' => $this->food->getGrams(),
			'ingredients' => $this->food->getIngredients(),
			'size' => $this->food->getSize()
		));
	}

	public function handleRemoveFood($foodId) {
		$food = $this->em->getRepository(Food::class)->find($foodId);
		$item = $food->getItem();
		$food->setItem(null);
		$this->em->flush();
		$this->em->remove($item);
		$this->em->remove($food);
		$this->em->flush();
		$this->flashMessage('Položka byla odstraňena.');
		$this->redirect('Item:');
	}
	public function handleRemoveBeverage($beverageId) {
		$beverage = $this->em->getRepository(Item::class)->find($beverageId);
		$item = $beverage->getItem();
		$beverage->setItem(null);
		$this->em->flush();
		$this->em->remove($item);
		$this->em->remove($beverage);
		$this->em->flush();
		$this->flashMessage('Položka byla odstraňena.');
		$this->redirect('Item:');
	}

	protected function createComponentAddFoodItemForm() {
		$form = new Form();
		$form->addHidden('edit');
		$form->addHidden('foodId');
		$form->addText('name', 'Název:')->setRequired('Zadejte název jídla.');
		$form->addSelect('category', 'Kategorie:')->setItems($this->foodCategories, false)->setRequired('Zvolte kategorii.');
		$form->addInteger('price','Cena (Kč):')->setRequired('Zadejte cenu.');
		$form->addInteger('grams', 'Množství (g):')->setRequired('Zadejte množství');
		$form->addTextArea('ingredients', 'Složení:');
		$form->addSelect('size', 'Porce:')->setItems($this->sizes, false)->setRequired('Zvolte velikost porce.');
		$form->addSubmit('send');
		$form->onSuccess[] = [$this, 'addFoodItemFormSucceeded'];
		return $form;
	}

	public function addFoodItemFormSucceeded(Form $form, Nette\Utils\ArrayHash $values) {
		if ($values->edit) {
			$food = $this->em->getRepository(Food::class)->find($values->foodId);
			$food->getItem()->setName($values->name);
			$food->getItem()->setPrice($values->price);
			$food->getItem()->setCategory($values->category);
			$food->setGrams($values->grams);
			$food->setSize($values->size);
			$food->setIngredients($values->ingredients);
			$this->flashMessage('Položka byla upravena.');
			$this->em->flush();
		}
		else {
			$item = new Item();
			$item->setName($values->name);
			$item->setCategory($values->category);
			$item->setPrice($values->price);
			$this->em->persist($item);
			$this->em->flush();
			$food = new Food();
			$food->setItem($item);
			$food->setGrams($values->grams);
			$food->setSize($values->size);
			$food->setIngredients($values->ingredients);
			$this->em->persist($food);
			$this->flashMessage('Položka byla vytvořena.');
			$this->em->flush();
		}
		$this->redirect('Item:');
	}

	protected function createComponentAddBeverageItemForm() {
		$form = new Form();
		$form->addHidden('edit');
		$form->addHidden('beverageId');
		$form->addText('name', 'Název:')->setRequired('Zadejte název.');
		$form->addSelect('category', 'Kategorie:')->setItems($this->beverageCategories, false)->setRequired('Zvolte kategorii.');
		$form->addInteger('price','Cena (Kč):')->setRequired('Zadejte cenu.');
		$form->addText('volume', 'Množství (l):')->setRequired('Zadejte množství.')->addRule(Form::FLOAT, 'Zadejte množství ve formáte desetinného čísla.');
		$form->addText('alcoholPercentage', 'Obsah alkoholu (%):')->setRequired('Zadejte obsah alkoholu.')->addRule(Form::FLOAT, 'Zadejte percento alkoholu číslo ve formáte desetinného čísla.');
		$form->addSubmit('send');
		$form->onSuccess[] = [$this, 'addBeverageItemFormSucceeded'];
		return $form;
	}

	public function addBeverageItemFormSucceeded(Form $form, Nette\Utils\ArrayHash $values){
		if ($values->edit) {
			$beverage = $this->em->getRepository(Beverage::class)->find($values->beverageId);
			$beverage->getItem()->setName($values->name);
			$beverage->getItem()->setPrice($values->price);
			$beverage->getItem()->setCategory($values->category);
			$beverage->setVolume($values->volume);
			$beverage->setAlcoholPercentage($values->alcoholPercentage);
			$this->flashMessage('Položka byla upravena.');
			$this->em->flush();
		}
		else {
			$item = new Item();
			$item->setName($values->name);
			$item->setCategory($values->category);
			$item->setPrice($values->price);
			$this->em->persist($item);
			$this->em->flush();
			$beverage = new Beverage();
			$beverage->setItem($item);
			$beverage->setVolume($values->volume);
			$beverage->setAlcoholPercentage($values->alcoholPercentage);
			$this->em->persist($beverage);
			$this->flashMessage('Položka byla vytvořena.');
			$this->em->flush();
		}
		$this->redirect('Item:');
	}

}