<?php


namespace App\Presenters;

use App\Model\User;
use Nette\Application\UI\Presenter;

class BasePresenter extends Presenter
{
	/**
	 * @inject
	 * @var \Kdyby\Doctrine\EntityManager
	 */
	public $em;
	protected $dao_user;
	protected $positions = ["waiter" => 'Číšník', "main_waiter" => 'Vedoucí číšník', "owner" => 'Majitel'];

	public function startup()
	{
		parent::startup();
		if (!$this->getUser()->isLoggedIn()) {
			$this->forward('Sign:in');
		}
		else {
			$this->dao_user = $this->em->getRepository(User::class)->find($this->user->getId());
		}
	}

	public function beforeRender()
	{
		if($this->user->isLoggedIn()){
			$this->template->dao_user = $this->dao_user;
			$this->template->positions = $this->positions;
		}
	}

	protected function sortBookings($bookings) {
		usort($bookings, function($a, $b) {
			return ($a->getDateTime() < $b->getDateTime()) ? -1 : 1;
		});
		return $bookings;
	}
}