<?php


namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;

class SignPresenter extends Nette\Application\UI\Presenter
{

	public function actionIn()
	{
		if ($this->getUser()->isLoggedIn()) {
			$this->forward('Homepage:');
		}
	}

	protected function createComponentSignInForm()
	{
		$form = new Form;
		$form->addText('username', 'Přihlašovací jméno', null, 50);
		$form->addPassword('password', 'Heslo');
		$form->addSubmit('send', 'Přihlásit');
		$form->onSuccess[] = [$this, 'signInFormSucceeded'];
		return $form;
	}

	public function signInFormSucceeded(Form $form, Nette\Utils\ArrayHash $values)
	{
		try {
			$this->getUser()->login($values->username, $values->password);
			$this->redirect('Homepage:');
		} catch (Nette\Security\AuthenticationException $e) {
			$form->addError('Nesprávné přihlašovací jméno nebo heslo.');
		}
	}

	public function actionOut()
	{
		$this->getUser()->logout();
		$this->flashMessage('Byli jste odhlášeni.', 'success');
		$this->forward('Homepage:');
	}

}