<?php

namespace App\Presenters;

use App\Model\Booking;
use App\Model\Tables;
use Nette\Application\UI\Form;
use Doctrine\ORM\EntityManager;
use Nette;


final class HomepagePresenter extends BasePresenter
{
	/**
	 * @inject
	 * @var \Kdyby\Doctrine\EntityManager
	 */
	public $em;
	private $dao_tables;
	private $dao_bookings;
	private $rooms = ['Hlavní', 'Salónek', 'Přední záhradka', 'Zadní záhradka',];

	public function renderDefault()
	{
		if (!empty($this->dao_tables))
			$this->template->tables = $this->dao_tables;
		if (!empty($this->dao_bookings))
			$this->template->bookings = $this->dao_bookings;

		$dateTime = new Nette\Utils\DateTime('today');
		$todaysBookings = [];
		foreach ($this->dao_bookings as $booking) {
			$interval = $dateTime->diff($booking->getDateTime());
			if ($interval->days == 0) {
				array_push($todaysBookings, $booking);
			}
		}
		$this->template->todaysBookings = $todaysBookings;
		$this->template->rooms = $this->rooms;
	}

	public function actionDefault()
	{
		$this->dao_tables = $this->em->getRepository(Tables::class)->findAll();
		$bookings = $this->em->getRepository(Booking::class)->findAll();
		$this->dao_bookings = $this->sortBookings($bookings);
	}

	public function handleRemoveBooking($bookingId)
	{
		$booking = $this->em->getRepository(Booking::class)->find($bookingId);
		$this->em->remove($booking);
		$this->em->flush();
		$this->dao_bookings = $this->em->getRepository(Booking::class)->findAll();
		$this->flashMessage('Rezervace byla zrušená.');
		if ($this->isAjax()) {
			$this->redrawControl('flash');
			$this->redrawControl('bookingArea');
			$this->redrawControl('todaysBookingsPanel');
		} else {
			$this->redirect('this');
		}
	}

	protected function createComponentAddTableForm()
	{
		$form = new Form;
		$capacities = ['2' => 2, '4' => 4, '6' => 6];
		$form->addRadioList('capacity', "Kapacita:", $capacities)->setRequired('Zvol kapacitu stolu.')->getSeparatorPrototype()->setName(null);
		$form->addSelect('room', 'Mistnost:')->setItems($this->rooms, false);
		$form->addSubmit('send', 'Přidat');
		$form->onSuccess[] = [$this, 'addTableFormSucceeded'];
		return $form;
	}

	public function addTableFormSucceeded(Form $form, Nette\Utils\ArrayHash $values)
	{
		$table = new Tables();
		$this->em->persist($table);
		$table->setCapacity($values->capacity);
		$table->setRoom($values->room);
		$this->em->flush();
		$this->flashMessage('Stůl byl přidán.', 'success');
		$this->forward('Homepage:');
	}

	protected function createComponentAddBookingForm()
	{
		$form = new Form;
		$tables = $this->em->getRepository(Tables::class)->findAll();
		$tableArray = [];
		foreach ($tables as $table) {
			array_push($tableArray, $table->getId());
		}
		$form->addCheckboxList('tables', 'Stoly:', $tableArray);
		$form->addText('customer', 'Zákazník:', null, 30)->setRequired();
		$form->addText('date', 'Datum');
		$form->addText('time', 'Čas');
		$form->addInteger('numberOfPeople', 'Počet lidí:')->addRule(Form::MIN, 'Počet lidí nemůže být záporné číslo.', 1)->setRequired();
		$form->addText('phoneNumber', 'Telefónní číslo:')->setRequired();
		$form->addSubmit('send', 'Přidat rezervaci');
		$form->onSuccess[] = [$this, 'addBookingFormSucceeded'];
		return $form;
	}

	public function addBookingFormSucceeded(Form $form, Nette\Utils\ArrayHash $values)
	{
		$dateTimeString = $values->date . " " . $values->time;
		$dateTime = Nette\Utils\DateTime::from($dateTimeString);
		$booking = new Booking();
		$booking->setCustomer($values->customer);
		$booking->setEmployee($this->dao_user->getEmployee());
		$booking->setDateTime($dateTime);
		$booking->setNumberOfPeople($values->numberOfPeople);
		$booking->setPhoneNumber($values->phoneNumber);
		$this->em->persist($booking);
		$this->em->flush();
		foreach ($values->tables as $table) {
			$dao_table = $this->em->getRepository(Tables::class)->find($table);
			$dao_table->addBooking($booking);
			$this->em->persist($dao_table);
			$this->em->flush();
		}
		$this->redirect('this');
	}
}
