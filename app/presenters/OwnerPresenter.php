<?php


namespace App\Presenters;

use Nette;
use App\Model\Employee;


class OwnerPresenter extends BasePresenter
{
	/**
	 * @inject
	 * @var \Kdyby\Doctrine\EntityManager
	 */
	public $em;
	private $dao_employees;

	public function beforeRender()
	{
		parent::beforeRender();
		if(!$this->user->isInRole('owner')){
			$this->flashMessage('Nemáte práva k prohlížení obsahu stránek. Přihlašte se jako majitel.');
			$this->redirect('Homepage:');
		}
	}

	public function renderEmployees() {
		$this->template->employees = $this->dao_employees;
	}

	public function actionEmployees()
	{
		$this->dao_employees = $this->em->getRepository(Employee::class)->findAll();
	}
}