<?php


namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;
use App\Model\Employee;
use App\Model\User;
use Nette\Security\Passwords;

class EmployeePresenter extends BasePresenter
{
	/**
	 * @inject
	 * @var \Kdyby\Doctrine\EntityManager
	 */
	public $em;
	private $dao_employees;
	private $dao_employee;

	public function beforeRender()
	{
		parent::beforeRender();
		if(!$this->user->isInRole('owner')){
			$this->flashMessage('Nemáte práva k prohlížení obsahu stránek. Přihlašte se jako majitel.');
			$this->redirect('Homepage:');
		}
	}

	public function renderDefault() {
		$this->template->employees = $this->dao_employees;
	}

	public function actionDefault()	{
		$this->dao_employees = $this->em->getRepository(Employee::class)->findAll();
	}

	public function actionCreate() {
		$this['addUserForm']->setDefaults(array(
			'edit' => false
		));
	}

	public function renderEdit($employeeId) {
		$this->template->employee = $this->dao_employee;
	}

	public function actionEdit($employeeId) {
		$this->dao_employee = $this->em->getRepository(Employee::class)->find($employeeId);
		$this['addUserForm']->setDefaults(array(
			'edit' => true,
			'employeeId' => $employeeId,
			'name' => $this->dao_employee->getName(),
			'surname' => $this->dao_employee->getSurname(),
			'position' => $this->dao_employee->getPosition(),
			'username' => $this->dao_employee->getUser()->getUsername(),
			'email' => $this->dao_employee->getUser()->getEmail()
		));
	}

	public function handleRemove($employeeId){
		$employee = $this->em->getRepository(Employee::class)->find($employeeId);
		$user = $employee->getUser();
		$user->setEmployee(null);
		$this->em->flush();
		$this->em->remove($employee);
		$this->em->remove($user);
		$this->em->flush();
		$this->flashMessage('Zaměstnanec byl odstraněn.');
		$this->redirect('this');
	}

	protected function createComponentAddUserForm()
	{
		$form = new Form;
		$form->addHidden('edit');
		$form->addHidden('employeeId');
		$form->addText('name', 'Jméno:', null, 20)->setRequired('Zadejte křestní jméno.');
		$form->addText('surname', 'Příjmení', null, 30)->setRequired('Zadejte přijmení.');
		$form->addSelect('position', 'Pozice:', $this->positions);
		$form->addText('username', 'Přihlašovací jméno:', null, 10)->setRequired('Zadejte přihlašovací jméno.');
		$form->addText('email', 'E-mail:', null, 50)->setRequired('Zadejte e-mail.');
		$form->addPassword('password', 'Heslo:')->setRequired('Zadejte heslo.');
		$form->addSubmit('send', 'Přidat zaměstnance');
		$form->onSuccess[] = [$this, 'addUserFormSucceeded'];
		return $form;
	}

	public function addUserFormSucceeded(Form $form, Nette\Utils\ArrayHash $values)
	{
		if ($values->edit) {
			$employee = $this->em->getRepository(Employee::class)->find($values->employeeId);
			$employee->setName($values->name);
			$employee->setSurname($values->surname);
			$employee->setPosition($values->position);
			$employee->getUser()->setUsername($values->username);
			$hash = Passwords::hash($values->password);
			$employee->getUser()->setPassword($hash);
			$employee->getUser()->setEmail($values->email);
			$this->em->flush();
			$this->flashMessage('Uživatel byl upraven.', 'success');
			$this->redirect('Employee:');
		}
		else {
			$sameUsername = $this->em->getRepository(User::class)->findBy(array('username' => $values->username));
			if (empty($sameUsername)) {
				$user = new User();
				$this->em->persist($user);
				$user->setUsername($values->username);
				$hash = Passwords::hash($values->password);
				$user->setPassword($hash);
				$user->setEmail($values->email);

				$employee = new Employee();
				$this->em->persist($employee);
				$employee->setName($values->name);
				$employee->setSurname($values->surname);
				$employee->setPosition($values->position);
				$employee->setUser($user);
				$user->setEmployee($employee);

				$this->em->flush();
				$this->flashMessage('Uživatel byl přidán.', 'success');
				$this->redirect('Employee:');
			} else {
				$form->addError('Přihlašovací jméno už existuje.');
			}
		}
	}

}