<?php


namespace App\Presenters;

use App\Model\OrderItem;
use App\Model\Orders;
use App\Model\Receipt;
use App\Model\Tables;
use App\Model\Item;
use Nette\Application\UI\Form;
use Doctrine\ORM\EntityManager;
use Nette;

class TablePresenter extends BasePresenter
{
	/**
	 * @inject
	 * @var \Kdyby\Doctrine\EntityManager
	 */
	public $em;
	private $dao_table;
	private $dao_items;
	private $beverage_categories = ['Pivo', 'Víno', 'Nealko', 'Alko', 'Horké'];
	private $food_categories = ['Hlavní', 'Příloha', 'Polévka', 'Pizza', 'Těstoviny'];
	private $paymentMethods = ['Kartou', 'Hotově'];
	private $totalToPay;

	public function renderDefault($tableId)
	{
		$this->template->beverage_categories = $this->beverage_categories;
		$this->template->food_categories = $this->food_categories;
		$this->template->table = $this->dao_table;

		if ($this->dao_table->getOrder()) {
			$this->template->order = $this->dao_table->getOrder();
		}

		if ($this->dao_table->getBookings()) {
			$bookingArray = [];
			foreach ($this->dao_table->getBookings() as $booking) {
				array_push($bookingArray, $booking);
			}
			$this->template->bookings = $this->sortBookings($bookingArray);
		}
		if ($this->totalToPay == null) {
			$this->totalToPay = 0;
		}
		$this->template->totalToPay = $this->totalToPay;
	}

	public function actionDefault($tableId)
	{
		if (!$this->getUser()->isLoggedIn()) {
			$this->forward('Sign:in');
		}
		$this->dao_table = $this->em->getRepository(Tables::class)->find($tableId);
		$this->dao_items = $this->em->getRepository(Item::class)->findAll();

		$this['addBeverageToOrderForm']->setDefaults(array(
			'table' => $tableId
		));
		$this['addFoodToOrderForm']->setDefaults(array(
			'table' => $tableId
		));
	}

	protected function createComponentPaymentForm() {
		$form = new Form();
		$orderItems = $this->dao_table->getOrder()->getOrderItems();
		foreach ($orderItems as $orderItem) {
			if ($orderItem->getPaid() < $orderItem->getCount()) {
				$form->addInteger($orderItem->getId(), $orderItem->getItem()->getName())->setDefaultValue(0)->addRule(Form::MIN, null, 0)->addRule(Form::MAX, null, $orderItem->getCount())->setRequired('Zadej počet položky.');
			}
		}
		$form->addRadioList('paymentMethod','Způsob platby:', $this->paymentMethods)->setRequired('Zadej způsob platby.');
		$form->addSubmit('send');
		$form->onSuccess[] = [$this, 'paymentSucceeded'];
		return $form;
	}

	public function paymentSucceeded(Form $form, Nette\Utils\ArrayHash $values) {
		$atLeastOneItemSelected = false;
		foreach ($values as $name => $value) {
			if (is_int($name)) {
				if ($value > 0) {
					$atLeastOneItemSelected = true;
				}
			}
		}

		if ($atLeastOneItemSelected) {
			$total = 0;
			$receipt = new Receipt();
			$receipt->setEmployee($this->dao_user->getEmployee());
			$receipt->setOrder($this->dao_table->getOrder());

			foreach ($this->dao_table->getOrder()->getOrderItems() as $orderItem) {
				if ($orderItem->getPaid() < $orderItem->getCount()) {
					$paid = $orderItem->getPaid();
					$toPay = $values[$orderItem->getId()];
					$total += $toPay * $orderItem->getItem()->getPrice();
					$orderItem->setPaid($paid + $toPay);
				}
			}

			$receipt->setSum($total);
			$receipt->setPaymentMethod($this->paymentMethods[$values->paymentMethod]);
			$this->em->persist($receipt);
			$this->em->flush();

			$unpaidOrderItemsCount = count($this->dao_table->getOrder()->getOrderItems());
			foreach ($this->dao_table->getOrder()->getOrderItems() as $orderItem) {
				if ($orderItem->getCount() <= $orderItem->getPaid()) {
					$unpaidOrderItemsCount--;
				}
			}
			if ($unpaidOrderItemsCount == 0) {
				$tables = $this->em->getRepository(Tables::class)->findBy(array('orders' => $this->dao_table->getOrder()));
				foreach ($tables as $table) {
					$table->setOrder(null);
					$this->em->flush();
				}
			}
			$this->flashMessage('Objednávka byla zaplacena.', 'success');
			$this->redirect('this');
		}
		else {
			$this->flashMessage('Vyberte položku k zaplacení.', 'error');

			if ($this->isAjax()) {
				$this->redrawControl('flash');
			} else {
				$this->redirect('this');
			}
		}
	}

	public function handleRemoveOrderItem($orderItemId) {
		$orderItem = $this->em->getRepository(OrderItem::class)->find($orderItemId);
		$order = $orderItem->getOrder();
		$count = $orderItem->getCount();
		if (count($order->getOrderItems()) == 1 && $orderItem->getCount() == 1) {
			$this->flashMessage('Položku objednávky nelze odstranit! V objednávce musí být alespoň 1 položka.', 'error');
		}
		else {
			$orderItem->setCount($count - 1);
			$this->em->persist($orderItem);
			if ($orderItem->getCount() == 0) {
				$order->getOrderItems()->removeElement($orderItem);
				$this->em->flush();
			}
			$this->flashMessage('Položka odstráněna.', 'success');
		$this->redirect('this');
		}
	}

	public function handleCancelOrder() {
		$this->em->remove($this->dao_table->getOrder());
		$this->dao_table->setOrder(null);
		$this->em->flush();
	}

	public function handleChangeVariable(array $values)
	{
		$total = 0;
		foreach ($this->dao_table->getOrder()->getOrderItems() as $orderItem) {
			if ($orderItem->getPaid() < $orderItem->getCount()) {
				$toPay = $values[$orderItem->getId()];
				$total += $toPay * $orderItem->getItem()->getPrice();
			}
		}
		$this->totalToPay = $total;
		if ($this->isAjax()) {
			$this->redrawControl('ajaxChange');
		}
	}

	protected function createComponentAddOrderToTables()
	{
		$dao_freeTables = $this->em->getRepository(Tables::class)->findBy(array('orders' => null));
		$tablesArray = [];
		foreach ($dao_freeTables as $freeTable) {
			array_push($tablesArray, $freeTable->getId());
		}
		$form = new Form();
		$form->addCheckboxList('freeTables', 'Volné stoly:', $tablesArray);
		$form->addSubmit('send');
		$form->onSuccess[] = [$this, 'addOrderToTablesSucceeded'];
		return $form;
	}

	public function addOrderToTablesSucceeded(Form $form, Nette\Utils\ArrayHash $values) {
		$order = $this->dao_table->getOrder();
		foreach ($values->freeTables as $key => $tableId) {
			$table = $this->em->getRepository(Tables::class)->find($tableId);
			$table->setOrder($order);
			$this->em->flush();
		}
		$this->flashMessage('Objednávka byla přirazena ke stolům.', 'success');
	}

	protected function createComponentAddBeverageToOrderForm()
	{
		$beverage_items = [];
		foreach ($this->dao_items as $item) {
			if (in_array($item->getCategory(), $this->beverage_categories))
				$beverage_items[$item->getId()] = $item;
		}
		$form = new Form;
		$form->addHidden('table');
		$form->addHidden('order');
		$form->addSelect('item', 'Nápoj:', $beverage_items);
		$form->addInteger('count', 'Množství:')->addRule(Form::MIN, 'Množství nemůže být záporné číslo.', 1)->setDefaultValue(1);
		$form->addSubmit('send', 'Přidat nápoj');
		$form->onSuccess[] = [$this, 'addItemToOrderSucceeded'];
		return $form;
	}

	protected function createComponentAddFoodToOrderForm()
	{
		$food_items = [];
		foreach ($this->dao_items as $item) {
			if (in_array($item->getCategory(), $this->food_categories))
				$food_items[$item->getId()] = $item;
		}
		$form = new Form;
		$form->addHidden('table');
		$form->addHidden('order');
		$form->addSelect('item', 'Jídlo:', $food_items);
		$form->addInteger('count', 'Množství:')->addRule(Form::MIN, 'Množství nemůže být záporné číslo.', 1)->setDefaultValue(1);
		$form->addSubmit('send', 'Přidat jídlo');
		$form->onSuccess[] = [$this, 'addItemToOrderSucceeded'];
		return $form;
	}

	public function addItemToOrderSucceeded(Form $form, Nette\Utils\ArrayHash $values)
	{
		$dao_item = $this->em->getRepository(Item::class)->find($values->item);
		$dao_table = $this->em->getRepository(Tables::class)->find($values->table);
		if (!$dao_table->getOrder()) {
			$order = new Orders();
			$order->setEmployee($this->dao_user->getEmployee());
			$this->em->persist($order);
			$dao_table->setOrder($order);
			$this->em->flush();

			$orderItem = new OrderItem();
			$orderItem->setCount($values->count);
			$orderItem->setItem($dao_item);
			$orderItem->setOrder($order);
			$orderItem->setPaid(0);
		}
		else {
			$orderItem = $this->em->getRepository(OrderItem::class)->findOneBy(array('item' => $values->item, 'orders' => $dao_table->getOrder()));
			if ($orderItem) {
				$count = $orderItem->getCount();
				$orderItem->setCount($count + $values->count);
			}
			else {
			$orderItem = new OrderItem();
			$orderItem->setCount($values->count);
				$orderItem->setItem($dao_item);
				$orderItem->setOrder($dao_table->getOrders());
				$orderItem->setPaid(0);
			}
		}
		$this->em->persist($orderItem);
		$this->em->flush();
		$this->flashMessage('Položka přidána.', 'success');
		$this->redirect('this');
	}

}