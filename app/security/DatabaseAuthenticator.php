<?php

namespace App\Security;

use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;
use Nette\Security\IIdentity;
use App\Model\User;
use Doctrine\ORM\EntityManager;
use Nette\Security\Passwords;

class DatabaseAuthenticator implements IAuthenticator
{
	//V presenterech používej @inject anotaci (proměnná musí být public)
	//V komponentách a službách konstruktor

	private $em;

	public function __construct(\Kdyby\Doctrine\EntityManager $em)
	{
	    $this->em = $em;
	}

	/**
	 * Performs an authentication against e.g. database.
	 * and returns IIdentity on success or throws AuthenticationException
	 * @return IIdentity
	 * @throws AuthenticationException
	 */
	function authenticate(array $credentials)
	{
		list ($username, $password) = $credentials;
		$dao_user = $this->em->getRepository(User::class)->findOneBy(array('username' => $username));
		if (empty($dao_user) or !(Passwords::verify($password, $dao_user->getPassword()))) {
			throw new AuthenticationException("Invalid username or password.");
		}
		return new Identity($dao_user->getId(), $dao_user->getEmployee()->getPosition(), []);
	}
}